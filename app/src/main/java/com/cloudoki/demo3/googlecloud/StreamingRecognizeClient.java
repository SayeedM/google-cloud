package com.cloudoki.demo3.googlecloud;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;

import com.google.cloud.speech.v1beta1.RecognitionAudio;
import com.google.cloud.speech.v1beta1.RecognitionConfig;
import com.google.cloud.speech.v1beta1.SpeechGrpc;
import com.google.cloud.speech.v1beta1.StreamingRecognitionConfig;
import com.google.cloud.speech.v1beta1.StreamingRecognitionResult;
import com.google.cloud.speech.v1beta1.StreamingRecognizeRequest;
import com.google.cloud.speech.v1beta1.StreamingRecognizeResponse;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

/**
 * Created by rmalta on 06/09/16.
 */
public class StreamingRecognizeClient {

    private int RECORDER_SAMPLERATE;
    private int RECORDER_CHANNELS;
    private int RECORDER_AUDIO_ENCODING;

    private String TAG = "StreamRecognizeClient";

    private int bufferSize = 0;

    private static final Logger logger = Logger.getLogger(StreamingRecognizeClient.class.getName());

    private final ManagedChannel channel;
    private final SpeechGrpc.SpeechStub speechClient;
    private StreamObserver<StreamingRecognizeResponse> responseObserver;
    private StreamObserver<StreamingRecognizeRequest> requestObserver;

    private AudioRecord recorder = null;

    private IResults mScreen;

    private volatile boolean isInit = false;

    public StreamingRecognizeClient(ManagedChannel channel, IResults screen) throws IOException {

        this.RECORDER_SAMPLERATE = 16000;
        this.RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
        this.RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

        this.bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

        this.channel = channel;
        this.mScreen = screen;

        speechClient = SpeechGrpc.newStub(channel);

        responseObserver = new StreamObserver<StreamingRecognizeResponse>() {
            @Override
            public void onNext(StreamingRecognizeResponse response) {
                    int numOfResults = response.getResultsCount();

                    if (numOfResults > 0) {

                        for (int i = 0; i < numOfResults; i++) {

                            StreamingRecognitionResult result = response.getResultsList().get(i);
                            String text = result.getAlternatives(0).getTranscript();

                            if (result.getIsFinal()) {
                                Log.d("\tFinal", text);
                                mScreen.onFinal(text);
                            } else {
                                Log.d("Partial", text);
                                mScreen.onPartial(text);
                            }

                        }
                    }
                }



            @Override
            public void onError(Throwable error) {
                Log.w(TAG, "Recognize failed: {0}", error);
                error.printStackTrace();

                if (error instanceof io.grpc.StatusRuntimeException){
                    System.out.println(((io.grpc.StatusRuntimeException)error).getStatus().getCode() == Status.Code.CANCELLED);
                }

                try {
                    shutdown(false);
                    if (error instanceof io.grpc.StatusRuntimeException && ((io.grpc.StatusRuntimeException)error).getStatus().getCode() != Status.Code.CANCELLED) {
                        mScreen.onError();
                    }
//                    if (error instanceof io.grpc.StatusRuntimeException && ((io.grpc.StatusRuntimeException)error).getStatus() != Status.CANCELLED){
//                        recognize();
//                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCompleted() {
                Log.i(TAG, "Recognize complete.");

                try {
                    shutdown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        requestObserver = speechClient.streamingRecognize(responseObserver);
    }

    public void shutdown() throws InterruptedException {
        shutdown(false);
    }

    public void shutdown(boolean closeChannel) throws InterruptedException {

        if( recorder != null ){
            recorder.stop();
            recorder.release();
            recorder = null;
        }



        if( closeChannel )
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /** Send streaming recognize requests to server. */
    public void recognize() throws InterruptedException, IOException {

        try {
            // Build and send a StreamingRecognizeRequest containing the parameters for
            // processing the audio.
            RecognitionConfig config =
                    RecognitionConfig.newBuilder()
                            .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                            .setSampleRate(this.RECORDER_SAMPLERATE)
                            .setLanguageCode("en-US")
                            .build();

            // Sreaming config
            StreamingRecognitionConfig streamingConfig =
                    StreamingRecognitionConfig.newBuilder()
                            .setConfig(config)
                            .setInterimResults(true)
                            .setSingleUtterance(false)
                            .build();
            // First request
            StreamingRecognizeRequest initial =
                    StreamingRecognizeRequest.newBuilder().setStreamingConfig(streamingConfig).build();



            requestObserver.onNext(initial);


            startRecordingJob();

        } catch (RuntimeException e) {
            // Cancel RPC.
            e.printStackTrace();
            requestObserver.onError(e);
            mScreen.onError();

        } catch (Exception ex){
            ex.printStackTrace();
        }
        // Mark the end of requests.
        try {
            requestObserver.onCompleted();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void startRecordingJob(){

        // Microphone listener and recorder
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                this.RECORDER_SAMPLERATE,
                this.RECORDER_CHANNELS,
                this.RECORDER_AUDIO_ENCODING,
                bufferSize);

        recorder.startRecording();

        byte[] buffer = new byte[bufferSize];
        int recordState;



        System.out.println("here t00");
         //loop through the audio samplings
            while ( (recordState = recorder.read(buffer, 0, buffer.length) ) > -1 ) {

                // skip if there is no data
                if( recordState < 0 )
                    continue;

                long now = System.currentTimeMillis();
                if (isHearingVoice(buffer, buffer.length)) {
                    lastVoiceHeard = now;
                    mScreen.onVoiceActivity(true);

                    buffer = amplify(buffer);
                }else {
                    mScreen.onVoiceActivity(false);
                    if (now - lastVoiceHeard >= SPEECH_TIMEOUT_MILLIS){
                        lastVoiceHeard = Long.MAX_VALUE;
                        System.out.println("timeout detected");
                        try {
                            shutdown(false);
                        }catch (InterruptedException ex){
                            ex.printStackTrace();
                        }

                        mScreen.onSpeechTimeout();
                        break;
                    }
                }


                // create a new recognition request
                StreamingRecognizeRequest request =
                        StreamingRecognizeRequest.newBuilder()
                                .setAudioContent(ByteString.copyFrom(buffer, 0, buffer.length))
                                .build();

                // put it on the works
                requestObserver.onNext(request);
            }

    }

    private byte [] amplify(byte [] buffer){
        int i = 0;
        while ( i < buffer.length ) {
            float sample = (float)( buffer[ i  ] & 0xFF
                    | buffer[i + 1] << 8 );

            // THIS is the point were the work is done:
            // Increase level by about 6dB:
            sample *= 2;
            // Or increase level by 20dB:
            // sample *= 10;
            // Or if you prefer any dB value, then calculate the gain factor outside the loop
            // float gainFactor = (float)Math.pow( 10., dB / 20. );    // dB to gain factor
            // sample *= gainFactor;

            // Avoid 16-bit-integer overflow when writing back the manipulated data:
            if ( sample >= 32767f ) {
                buffer[i] = (byte)0xFF;
                buffer[i+1] =       0x7F;
            } else if ( sample <= -32768f ) {
                buffer[i] =       0x00;
                buffer[i+1] = (byte)0x80;
            } else {
                int s = (int)( 0.5f + sample );  // Here, dithering would be more appropriate
                buffer[i] = (byte)(s & 0xFF);
                buffer[i+1] = (byte)(s >> 8 & 0xFF);
            }
            i += 2;
        }
        return buffer;
    }


    private static final int AMPLITUDE_THRESHOLD = 6000; //1500;
    private static final int SPEECH_TIMEOUT_MILLIS = 3000;
    private static final int MAX_SPEECH_LENGTH_MILLIS = 30 * 1000;
    private long lastVoiceHeard = Long.MAX_VALUE;

    private boolean isHearingVoice(byte[] buffer, int size) {
        for (int i = 0; i < size - 1; i += 2) {
            // The buffer has LINEAR16 in little endian.
            int s = buffer[i + 1];
            if (s < 0) s *= -1;
            s <<= 8;
            s += Math.abs(buffer[i]);
            if (s > AMPLITUDE_THRESHOLD) {
                System.out.println(s);
                return true;
            }
        }
        return false;
    }


}
