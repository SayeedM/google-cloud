package com.cloudoki.demo3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudoki.demo3.googlecloud.IResults;
import com.cloudoki.demo3.googlecloud.MicrophoneStreamRecognizeClient;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements IResults {

    private IResults Self = this;
    private TextView textView, status;

    private Button startButton, stopButton;

    MicrophoneStreamRecognizeClient client;

    private class Runner implements Runnable{
        public void run(){

            try {
                Log.d("Main Activity", "Start");
                long start = System.currentTimeMillis();
                if (client == null)
                    client = new MicrophoneStreamRecognizeClient(getResources().openRawResource(R.raw.google), Self);
                else
                    client.reinitStream(Self);
                long end = System.currentTimeMillis();
                onReady();
                System.out.println("Took " + (end - start) + " ms to init");

                client.start();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private Thread runner;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        status = (TextView)findViewById(R.id.status);

        // checking permissions
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            int RECORD_AUDIO = 666;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    RECORD_AUDIO);
        }

        permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            int ACCESS_NETWORK_STATE = 333;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                    ACCESS_NETWORK_STATE);
        }


        startButton = (Button) findViewById(R.id.startStreaming);
        stopButton = (Button) findViewById(R.id.stopStreaming);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("here");
                startButton.setVisibility(View.GONE);

                status.setText("Connecting");
                runner = new Thread(new Runner());
                runner.start();
            }
        });


        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Log.d("Main Activity", "Stop");
                    client.stop();
                    runner.join();
                    status.setText("Interrupted");
                    startButton.setVisibility(View.VISIBLE);
                    stopButton.setVisibility(View.GONE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setText(String text){
        runOnUiThread(new Runnable() {

            String text;
            public Runnable set(String text) {
                this.text = text;
                return this;
            }

            @Override
            public void run() {

                if(textView != null){
                    textView.setText(text);

                }
            }

        }.set(text));
    }

    @Override
    public void onPartial(String text) {

        setText("Partial: "+text+"\n");
    }

    @Override
    public void onFinal(final String text) {
        try {
            client.stop();
            runner.join();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    status.setText("Done");
                    startButton.setVisibility(View.VISIBLE);
                    stopButton.setVisibility(View.GONE);

                    textView.setText("Final: " + text + "\n");

                }
            });

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onVoiceActivity(final boolean voicePresent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (voicePresent)
                    status.setText("Talking");
                else
                    status.setText("Silence");
            }
        });
    }

    @Override
    public void onReady() {
        System.out.println("on Ready");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Speak Now");
                stopButton.setVisibility(View.VISIBLE);
//                stopButton.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Error");
                startButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onSpeechTimeout() {
        System.out.println("onSpeechTimeout");
        try {
            //runner.join();

            System.out.println("stopped the clients");



            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    status.setText("Timeout");
                    startButton.setVisibility(View.VISIBLE);
                    stopButton.setVisibility(View.GONE);

                }

            });
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
